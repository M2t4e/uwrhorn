#!/bin/bash


# Confim command
###################################
confirm() {
    # call with a prompt string or use a default
    read -r -p "${1:-Are you sure? [y/N]} " response
    case "$response" in
        [yY][eE][sS]|[yY]) 
            true
            ;;
        *)
            false
            ;;
    esac
}

###################################
printf "\nMQTT (moquitto)\n"
printf "===============\n"
###################################

# create dir if doesn't exist and enter
if [ ! -d "./mqtt" ]; then
    mkdir "./mqtt"
fi
cd "./mqtt"

#if file exists, ask to delete of change
if [ -f "passwd" ]; then
    printf "\nThere already exists a password file for the mqtt passwords.\n"
    if confirm "Change passwords? [y/N]"; then
        rm "passwd"
    fi
fi
# if doesn't exists or delted, create
if [ ! -f "passwd" ]; then
    printf "\nCreate password for mqtt users.\n"
    # ask for new passwords
    GameController_PASSWORD_1="1"
    GameController_PASSWORD_2="2"
    while [ ! "$GameController_PASSWORD_1" = "$GameController_PASSWORD_2" ]; do
        printf "\nUsername: GameController\n"
        read -s -p "New password: " GameController_PASSWORD_1
        printf "\n"
        read -s -p "Re-type new password: " GameController_PASSWORD_2
        printf "\n"
        if [ ! "$GameController_PASSWORD_1" = "$GameController_PASSWORD_2" ]; then
            printf "The passwords didn't match. Try again.\n"
        fi
    done
    GameControllerUI_PASSWORD_1="1"
    GameControllerUI_PASSWORD_2="2"
    while [ ! "$GameControllerUI_PASSWORD_1" = "$GameControllerUI_PASSWORD_2" ]; do
        printf "\nUsername: GameControllerUI\n"
        read -s -p "New password: " GameControllerUI_PASSWORD_1
        printf "\n"
        read -s -p "Re-type new password: " GameControllerUI_PASSWORD_2
        printf "\n"
        if [ ! "$GameControllerUI_PASSWORD_1" = "$GameControllerUI_PASSWORD_2" ]; then
            printf "The passwords didn't match. Try again.\n"
        fi
    done
    # create mosquitto passowrd file
    printf "GameController:$GameController_PASSWORD_1\nGameControllerUI:$GameControllerUI_PASSWORD_1\n" > passwd
    mosquitto_passwd -U passwd
    # change python file
    cp ../../python_scripts/hornconfig.py.example ../../python_scripts/hornconfig.py
    sed -i "s/mqtt_password = '.*'$/mqtt_password = '$GameController_PASSWORD_1'/" ../../python_scripts/hornconfig.py
    # change javascript file
    cp ../../htdocs/private/assets/js/uwrhorn-config-private.js.example ../../htdocs/private/assets/js/uwrhorn-config-private.js
    sed -i "s/password: '.*',$/password: '$GameControllerUI_PASSWORD_1',/" ../../htdocs/private/assets/js/uwrhorn-config-private.js
fi

# exit dir
cd "../"

###################################
printf "\n\nWEBPAGE(nginx)\n"
printf "==============\n\n"
###################################

# create dir if doesn't exist and enter
if [ ! -d "./webserver" ]; then
    mkdir "./webserver"
fi
cd "./webserver"

#if file exists, ask to delete of change
if [ -f "passwd_webpage" ]; then
    printf "There already exists a password file for the webpage ui.\n"
    if confirm "Delete and recreate this file? [y/N]"; then
        rm "passwd_webpage"
    else
        if confirm "Add or change user? [y/N]"; then
            read -r -p "Username: " username
            htpasswd "passwd_webpage" "$username"
        fi
    fi
fi
# if doesn't exists or delted, create
if [ ! -f "passwd_webpage" ]; then
    printf "Create password file for the web ui.\n"
    read -r -p "Username: " username
    htpasswd -c "passwd_webpage" "$username"
fi

# exit dir
cd "../"
