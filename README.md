# Underwater Rugby Horn

Program for the time and sound management of an underwater rugby game. It
is intended to run on a RaspberryPi.

## Discord Server

Please join our Discord server if you have any questions or just want to get in contact:  
[UWR Horn Discord Server](https://discord.gg/AV9d575CYK)


## Installation, startup and updating

Installation steps are described in the project's wiki:  
https://gitlab.com/UnderWaterRugbyHorn/uwrhorn/-/wikis/Installation#software-installation

How to update is described there as well:  
https://gitlab.com/UnderWaterRugbyHorn/uwrhorn/-/wikis/Installation#software-updates

## Set up raspberry pi from scratch

- download current version of raspbian: https://www.raspberrypi.org/downloads/raspbian/
  - Recommention: Use the version with x-server, but without extra software, ie. the middle one in size.
- Follow the install guide on the download page.
  - in short: `unzip -p <downloaded raspbian file>.zip | sudo dd of=/dev/sdX bs=4M conv=fsync`
  - Useful commands: `lsblk -p`, `sudo umount /dev/sdX`
- Insert SD card into RPi. Redo the previous step, it boot up fails.
- Connect Display and keyboard.
- Connect power supply to boot up RPi.
- Select country and language.
- Select password for user pi.
  - Tip: It may still be US keyboard layout, so unhide the password
- Select WiFi network and connect to it
  - Tip: It may still be US keyboard layout, so unhide the password
- Update the RPi.
- Reboot the RPi.
- Open RPi Configuration:
  - System -> Hostname: uwr
  - Interfaces -> SSH: Enable
  - Interfaces -> I2C: Enable
  - Localisation: Choose keyboard layout
- Load clock module by adding to the end of the file `sudo nano /boot/config.txt`
```
dtoverlay=i2c-rtc,pcf8523
```
https://pimylifeup.com/raspberry-pi-rtc/
- Reboot RPi again
- From here on you can also input all the commands via ssh
  - `ssh pi@uwr` or `ssh pi@uwr.local`
  - if the hostname `uwr` cannot be resolved, sometime reboot the RPi helps, or add the ip of the RPi to your /etc/hosts
- Follow the general UWR Horn installation instructions: https://gitlab.com/UnderWaterRugbyHorn/uwrhorn/-/wikis/Installation#software-installation
- Autostart browser in fullscreen:  
  add the followin line to the end of the file `sudo nano /etc/xdg/lxsession/LXDE-pi/autostart`:
  ```
  @chromium-browser --start-fullscreen --disable-restore-session-state https://localhost/private/controller.html
  ```

## Set up uwr router

The following settings create two WiFis, `uwr` and `uwr-admin`. Both WiFis can connect to the RPi. It can be reached under `uwr.de` The WiFi `uwr` is open, ie. it has no password. The WiFi `uwr-admin` is password protected. There is also a possibility to add a local router to the ethernet port `Wan`. If the local router has an internet connection, both the RPi and everyone connected to `uwr-admin` will have internet access as well. Everyone connected to `uwr` will not have internet access.

If someone us connected to one of the WiFis with a smartphone, she or he needs to switch off mobile data. Otherwise a connection cannot be established.

- Connect 5V power supply.
- Connect RPi to `LAN` ethernet port.
- Open a browser on the RPi and go to the address `192.168.8.1`.
- Select language and router password.
- Go to `INTERNET` settings:
  - Confirm that in the box `cable` the text `No cable detected in WAN. ...` is present.
  - Connect ethernet cable from you local router to the `WAN` ethernet port on the uwr router.
  - select `DHCP`.
- Go to `UPGRADE`:
  - If it is possible to update the router, do so. Then start from the beginning.
- Go to `WIRELESS` settings and set them as follows:
  - 2.4G WiFi:
    - SSID: `uwr-admin`
    - Set password
  - 2.4G Guest WiFi
    - SSID: `uwr`
    - WiFi-Security: `OPEN`
  - Switch both WiFis on.
- Go to `MORE SETTINGS` -> `Advanced`.
- Type in the router password in the new tab.
- Go to `System` -> `System`:
  - Set hostname to `uwrrouter`.
  - Click and `Save & Apply`. It might take a moment before you can reach the interface again.
- Open a console in the RPi and type `ifconfig eth0`.
- Remember the 12 numbers and letters behind the word ether.
- In the advanced settings go to `Network` -> `DHCP and DNS` and then `General Settings`:
  - At the bottom add a static Lease:
    - Hostname: `uwr`
    - MAC-Address: Select the numbers and letters you remembered in the console.
    - IPv4-Address: `192.168.8.2`
    - Lease time: `infinite`
    - Leave the last two options blank.
  - Click `Save & Apply`.
- Go to `Network` -> `Hostnames`:
  - Change `console.gl-inet.com` to `uwrrouter.de`.
  - Add another hostname:
    - Hostname: `uwr.de`
    - IP Address: `192.168.8.2`
  - Click `Save & Apply`.
- The last two steps can also be done for a Wi-Fi printer.
- Go to `Network` -> `Firewall`:
  - In the tab `General Settings`:
    - At the bottom click `Edit` in the `guestzone` row.
    - In the `destination` field change `wan` to only `lan`.
    - Click `Save & Apply`.
  - In the tab `Port Forwards`:
    - Add the first new port forward:
      - Name: `wan_http`
      - Protocol: `TCP + UDP`
      - External zone: `wan`
      - External port: `80`
      - Internal zone: `lan`
      - Internal IP address: `192.168.8.2`
      - Internal port: `80`
    - Add a second with the Name `wan_mqtt` and port `1883`. The rest is the same.
    - Add a third with the Name `wan_websockets` and port `1884`. The rest is the same.
    - Add a fourth with the Name `wan_mqtt_sec` and port `8883`. The rest is the same.
    - Add a fifth with the Name `wan_websockets_sec` and port `8884`. The rest is the same.
    - Add a sixth  with the Name `wan_ssh` and port `22`. The rest is the same.
    - Click `Save & Apply`.
- On your local router change the name of the uwr router to `uwr`. You can find it either under the name `uwr2` or `GL-MT300N-V2-959`.

## set hardware clock
- `sudo date -s '2018-08-23 22:56'`
- `sudo hwclock -w`
- `sudo hwclock -r`

## Some networking commands
- `sudo raspi-config`
- `sudo iwlist wlan0 scan`

## see the logging info
- see complete logs of curent boot `journalctl -u uwrhorn`
- monitor logs live: `journalctl -f -u uwrhorn`
- between some time: `journalctl --since "2015-01-10" --until "2015-01-11 03:00" -u uwrhorn`

## if internet is needed
The raspberry pi can be conected to the internet either via the router'S WAN port or via WIFI. Choose one one the following methods:
- Connect WAN of UWR-router to a local network with an internet connection.  
  All users connected to the `uwr-admin` WLan network will then have internet acces as well. All users connected to the open `uwr` WLan networw will not have access.
- Connect to a WLan and switch off the ethernet connection
  - Use the touch screen to connect to a WLan
  - SSH into the raspberry: `ssh pi@uwr.local`
  - Switch off ethernet: `sudo ifconfig eth0 down`
  - Reconnect ethernet after you are finished:
    - `sudo ifconfig eth0 up`
- Change the routing metric
  - Run `ip route show` and remember the ip of the wlan0 gateway
  - Add a new default route with lower metric then eth0: `sudo ip route add default via 192.168.10.1 metric 101`
- Last time I tried this didn't work
  - Add `8.8.8.8` to `/etc/resolve.conf` on pi
  - https://netbeez.net/blog/linux-how-to-resolve-a-host-and-test-dns-servers/

## images of SD card
- `lsblk`
- `sudo dd if=/dev/sdc | pv -s 15G> backup.img`
- `truncate`
- `gzip`
- as su: `cat backup.img.gz | gunzip | pv -s 9G | dd of=/dev/sdc`

## printer
- printer port: uwr.local:631
- set default printer
    - find printers: lpstat -p -d
    - set default: lpoptions -d <printer>
- test printer: lpq

## delete persistence file
Persisitance is currently switched off!
- `sudo rm /var/lib/mosquitto/mosquitto.db`
- `sudo service mosquitto restart`

## Client Certificate UWR
This is not used anymore!
- create new client cert
  ```
  openssl genrsa -out alice.key 2028
  openssl req -new -key alice.key -out alice.csr
  openssl +509 -sha256 -req -in alice.csr -out alice.crt -CA /etc/apache2/ssl/server.crt -CAkey /etc/apache2/ssl/server.key -CAcreateserial -days 9999
   openssl pks13 -export -out alice.pfx -inkey alice.key -in alice.crt
   ```
- use this client cert
    - `client_uwrhorn.pfx`
    - passwort: `uwrhorn`

## accept cert in firefox:
This isn't necessary any more!
- accept cert
- settings -> advanced -> view certs
    - install client.cert
    - find CA uwr.local
    - allow everything for this CA

## mosquitto commands
- remove a retained message: `mosquitto_pub -h uwr -u "GameController" -P "<password>" -t "command/answer" -r -n`
- listen to all messages: `mosquitto_sub -h uwr -u "GameController" -P "<password>" -t "#" -v`
