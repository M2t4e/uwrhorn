mqtt_config["topics"] = ["tournament/delay",
                         "tournament/gameday",
                         "tournament/roster",
                         "tournament/settings",
                         "state/real_time",
                         "state/game_time",
                         "state/game_state",
                         "state/game_config"];

$(document).ready(function() {
    //$('#uwrhorn-navbar').load('/navbar.html');
    
    // set options for gameday table
    $gamedaytable = $('#gamedaytable');
    $gamedaytable.bootstrapTable({
        classes: "table table-hover table-no-bordered",
        iconsPrefix: 'fa',
        idField: 'id',
        uniqueId: 'id',
        undefinedText: '',
        search: true,
        searchAlign: 'left',
        trimOnSearch: false,
        striped: true,
        sortName: 'plan',
        sortOrder: 'asc',
        rowStyle: rowStyle,
        columns: [
            {field: 'id',
             title: '#',
             sortable: true},
            {field: 'category',
             title: 'Category:',
             formatter: category_formatter,
             width: 20
            },
            {field: 'gameType',
             title: 'Game Type:',
             formatter: gameType_formatter,
             width: 20
            },
            {field: 'plan',
             title: 'Plan',
             formatter: plan_formatter,
             sortable: true},
            {field: 'time',
             title: 'Time',
             formatter: time_formatter},
            {field: 'blue',
             title: 'Blue',
             formatter: teamname_formatter},
            {field: 'white',
             title: 'White',
             formatter: teamname_formatter},
            {field: 'score',
             title: 'Score'}]
    });
    $gamedaytable.bootstrapTable('showLoading');

    MQTTconnect();
});

function rowStyle(row, index) {
    if (!game_config) return {};
    if (row.id == game_config.game_id) {
        return {classes: 'table-warning'};
    }
    return {};
}

mqtt_messages.onGameday = function (topic, payload) {
    MQTTdefaultMessages.prototype.onGameday.call(this, topic, payload);
    $gamedaytable.bootstrapTable('load', gameday);
    $gamedaytable.bootstrapTable('hideLoading');
};

mqtt_messages.onRoster = function (topic, payload) {
    MQTTdefaultMessages.prototype.onRoster.call(this, topic, payload);
    $gamedaytable.bootstrapTable('load', gameday);
};

mqtt_messages.onGameConfig = function (topic, payload) {
    MQTTdefaultMessages.prototype.onGameConfig.call(this, topic, payload);
    $gamedaytable.bootstrapTable('load', gameday);
};
