mqtt_config["topics"] = ["state/real_time",
                         "state/game_time",
                         "state/game_state",
                         "state/game_config",
                         "tournament/settings"];

$(document).ready(function() {
    MQTTconnect();
});


mqtt_messages.onGameState = function (topic, payload) {
    MQTTdefaultMessages.prototype.onGameState.call(this, topic, payload);
    if (game_state.n_halftime % 2) {
        $('.RightBox').addClass("Blue").removeClass("White");
        $('.LeftBox').removeClass("Blue").addClass("White");
        $('#score_left').html(game_state.score_white);
        $('#score_right').html(game_state.score_blue);
        if (game_config) {
            $('#teamname_left').html(game_config.teamname_white);
            $('#teamname_right').html(game_config.teamname_blue);
        }
    }
    else {
        $('.RightBox').removeClass("Blue").addClass("White");
        $('.LeftBox').addClass("Blue").removeClass("White");
        $('#score_right').html(game_state.score_white);
        $('#score_left').html(game_state.score_blue);
        if (game_config) {
            $('#teamname_right').html(game_config.teamname_white);
            $('#teamname_left').html(game_config.teamname_blue);
        }
    }
    $('#game_time_left').html("H"+game_state.n_halftime);
    set_powerplay_table();
};

mqtt_messages.onGameConfig = function (topic, payload) {
    MQTTdefaultMessages.prototype.onGameConfig.call(this, topic, payload);
    if (game_state) {
        if (game_state.n_halftime % 2) {
            $('.RightBox').addClass("Blue").removeClass("White");
            $('.LeftBox').removeClass("Blue").addClass("White");
            $('#score_left').html(game_state.score_white);
            $('#score_right').html(game_state.score_blue);
            $('#teamname_left').html(game_config.teamname_white);
            $('#teamname_right').html(game_config.teamname_blue);
        }
        else {
            $('.RightBox').removeClass("Blue").addClass("White");
            $('.LeftBox').addClass("Blue").removeClass("White");
            $('#score_right').html(game_state.score_white);
            $('#score_left').html(game_state.score_blue);
            $('#teamname_right').html(game_config.teamname_white);
            $('#teamname_left').html(game_config.teamname_blue);
        }
        $('#game_time_left').html("H"+game_state.n_halftime);
    }
};

mqtt_messages.onGameTime = function (topic, payload) {
    MQTTdefaultMessages.prototype.onGameTime.call(this, topic, payload);
    let timetext = "";
    let timetextright= "";
    if (game_state != null && game_state.is_timeout) {
        timetext = get_time_text();
        timetextright = "Timeout";
    }
    else if (game_state != null && game_state.is_penaltythrow) {
        if (tournamentSettings.show_penalty_time) {
            timetext = get_time_text();
            timetextright = "Penalty";
        }
        else {
            timetext = "Penalty";
        }
    }
    else if (game_state != null && game_state.is_halftime_break) {
        $('#game_time_left').html("Halftime");
        timetext = get_time_text();
    }
    else if (game_state != null) {
        $('#game_time_left').html("H"+game_state.n_halftime);
        timetext = get_time_text();
    }
    else {
        timetext = get_time_text();
    }
    if (timetext != $('#game_time').html()) {
        $('#game_time').html(timetext);
    }
    if (timetextright != $('#game_time_right').html()) {
        $('#game_time_right').html(timetextright);
    }
    set_powerplay_table();
};

function set_powerplay_table() {
    if (!linked_timers) return;
    if (!game_state) return;
    let powerplay_table_blue = "";
    let powerplay_table_white = "";
    Object.keys(linked_timers).forEach(function(key,index) {
        if (linked_timers[key].active) {
            let new_entry = `
<div class="progress position-relative">
    <div class="progress-bar bg-danger" aria-valuenow="${linked_timers[key].percentage}" aria-valuemin="0" aria-valuemax="100" style="width: ${linked_timers[key].percentage}%;">
        <small class="justify-content-center d-flex position-absolute w-100" style="color: #212529;">
            ${linked_timers[key].countdown}
        </small>
    </div>
</div>`;
            if (linked_timers[key].teamcolor == 'blue') {
                powerplay_table_blue += new_entry;
            } else if (linked_timers[key].teamcolor == 'white') {
                powerplay_table_white += new_entry;
            }
        }
    });
    if (game_state.n_halftime % 2) {
        if (powerplay_table_blue != $('#powerplay_table_right').html()) {
            $('#powerplay_table_right').html(powerplay_table_blue);
        }
        if (powerplay_table_white != $('#powerplay_table_left').html()) {
            $('#powerplay_table_left').html(powerplay_table_white);
        }
    } else {
        if (powerplay_table_white != $('#powerplay_table_right').html()) {
            $('#powerplay_table_right').html(powerplay_table_white);
        }
        if (powerplay_table_blue != $('#powerplay_table_left').html()) {
            $('#powerplay_table_left').html(powerplay_table_blue);
        }
    }
};
