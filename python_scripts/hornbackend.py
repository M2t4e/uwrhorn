#!/usr/bin/env python
# -*- coding: utf-8 -*-

'''
The module horn provides the backend for the time management. Run the *main.py*
script to start the backend. The *main.py* script will restart this one after
a crash. If the backend should not be restarted after a crash, run this script
directly.'''

import os
import time
import json
import logging
import paho.mqtt.client as mqtt
import horngame
import hornbuttons
import hornconfig
import hornprinting
import threading
import datetime
import traceback
import dateutil.parser

Logger = logging.getLogger(__name__)


class Backend():
    ''' Backend of the underwater rugby horn.

    This class needs to do some cleanup when it closes. It should therefore be
    used as follows::

        backend = hornbackend.Backend()
        with backend:
            # do something with backend, e.g. infinite loop

    This will load the old log from previous instances of this class to
    continue a game after a crash of the raspberry pi. On
    exit it will stop and close all remaining timer threads.
    '''

    def __init__(self):
        Logger.info('initialise Backend object ' + str(id(self)))

        self.gameday = []
        self.lineup = {}
        self.roster = {}
        self.tournament_settings = {
            "name": "Tournament Name",
            "town": "",
            "pool": "",
            "protocol_file": "protocol.tex",
            "use_lineup": False,
            "show_penalty_time": False,
            "german_events": False
        }

        self.plan_game_start_list = []

        self.last_saved_tounament_state = ''
        self.last_saved_game_state = ['', '']
        self.last_published = {}

        self.keep_save_state_to_log_loop_running = True
        self.keep_publish_time_loop_running = True

        # initialise mqtt client
        self.mqtt_client = mqtt.Client(
            callback_api_version=mqtt.CallbackAPIVersion.VERSION2,
            client_id="python_backend",
            clean_session=True,
        )
        self.mqtt_client.username_pw_set(hornconfig.mqtt_username,
                                    hornconfig.mqtt_password)
        # self.client.tls_set(hornconfig.mqtt_cert)
        self.mqtt_client.on_connect = self.on_connect
        self.mqtt_client.on_message = self.on_message
        self.mqtt_client.on_publish = self.on_publish

        self.mqtt_publish_lock = threading.Lock()

        self.game = horngame.Game(self.mqtt_publish, self.sound_end_sequence, self.update_score_gameday)

        self.printer = hornprinting.Printer(self.mqtt_publish)

        # initialise the referee buttons
        self.button_listener = hornbuttons.ButtonListener(
            self.game.stop_game,
            self.game.start_game,
            hornconfig.sequence_detection_time,
            self.mqtt_publish)

        # initialise polling threads
        self.save_to_log_thread = threading.Thread(
            target=self.save_state_to_log_loop)
        self.save_to_log_thread.daemon = True
        self.publish_time_thread = threading.Thread(
            target=self.publish_time_loop)
        self.publish_time_thread.daemon = True

    def __enter__(self):
        Logger.info('enter Backend object ' + str(id(self)))
        passed = self.load_log(hornconfig.tournament_fname,
                               hornconfig.tournament_fname_backup,
                               self.set_tournament_state)
        if not passed:
            self.reset_all()
        passed = self.load_log(hornconfig.current_game_fname,
                               hornconfig.current_game_fname_backup,
                               self.game.set_state)
        if not passed:
            self.game.reset_game(check_sanity=False, start_new_game=True)
        # connect to client
        self.mqtt_client.loop_start()
        self.mqtt_client.connect_async(hornconfig.mqtt_hostname,
                                       hornconfig.mqtt_port,
                                       60)

        # start polling thread that saves to log
        if not self.save_to_log_thread.is_alive():
            self.save_to_log_thread.start()

    def __exit__(self, type, value, traceback):
        Logger.info('exit Backend object ' + str(id(self)))
        self.mqtt_client.loop_stop()
        self.mqtt_client.disconnect()
        # TODO: return something more sensible than 'None'

    def reset_all(self):
        '''This function resets everything no matter the state. It also deletes
        the old states, which are saved on the disk and starts from a new. This
        function should only be used for developing purposes.
        '''
        Logger.debug('reset all')
        self.game.reset_game(check_sanity=False, start_new_game=True)
        self.game.saved_states = {}
        self.gameday = []
        self.lineup = {}
        self.roster = {}
        self.tournament_settings = {
            "name": "Tournament Name",
            "town": "",
            "pool": "",
            "protocol_file": "protocol.tex",
            "use_lineup": False,
            "show_penalty_time": False,
            "german_events": False
        }
        self.plan_game_start_list = []

        self.last_saved_tounament_state = ''
        self.last_saved_game_state = ['', '']
        self.last_published = {}

        if os.path.isfile(hornconfig.current_game_fname):
            os.remove(hornconfig.current_game_fname)
        if os.path.isfile(hornconfig.current_game_fname_backup):
            os.remove(hornconfig.current_game_fname_backup)

        if os.path.isfile(hornconfig.tournament_fname):
            os.remove(hornconfig.tournament_fname)
        if os.path.isfile(hornconfig.tournament_fname_backup):
            os.remove(hornconfig.tournament_fname_backup)

        self.save_tournament_to_log()

        self.game.publish_all()
        self.mqtt_publish('tournament/gameday', self.gameday, retain=True)
        self.mqtt_publish('tournament/delay', {}, retain=True)
        self.mqtt_publish('tournament/lineup', self.lineup, retain=True)
        self.mqtt_publish('tournament/roster', self.roster, retain=True)
        self.mqtt_publish('tournament/settings', self.tournament_settings, retain=True)

    def on_connect(self, client, userdata, flags, reason_code, properties):
        '''The callback for when the client receives a CONNACK response from the
        server.
        Subscribing in on_connect() means that if we lose the connection and
        reconnect then subscriptions will be renewed.'''
        try:
            if reason_code == 0:
                Logger.info(f"Connected with reason code: {reason_code}")
                # publish loaded state before they get overwritten with what is
                # still in the mqtt broker
                self.game.publish_all()
                self.mqtt_publish('tournament/gameday', self.gameday, retain=True)
                self.mqtt_publish('tournament/roster', self.roster, retain=True)
                self.mqtt_publish('tournament/lineup', self.lineup, retain=True)
                self.mqtt_publish('tournament/settings', self.tournament_settings, retain=True)
            # subscribe to topics
                self.mqtt_client.subscribe("command/#", 1)
                self.mqtt_client.message_callback_add("command/#", self.on_command)
                self.mqtt_client.subscribe("tournament/#", 1)
                self.mqtt_client.message_callback_add("tournament/#", self.on_command)
                # start polling threads
                if not self.publish_time_thread.is_alive():
                    self.publish_time_thread.start()
            else:
                Logger.error(f"Connection failed with reason code: {reason_code}")
        except Exception as e:
            Logger.error("VERY BAD! Something wrong happened in the mqtt on_connect call. "
                         "You probably have to fix the code to get it running again.\n"
                         f"{e}\n\n{traceback.format_exc()}")

    def on_message(self, client, userdata, msg):
        '''The callback for when a PUBLISH message is received from the server.
        This function is only called if the subscription has no special
        callback.'''
        Logger.info('received message (' + str(msg.topic) + "):" + str(msg.payload))

    def on_command(self, client, userdata, msg):
        '''The callback for all subscriptions, whose topic starts with
        "command/". It will call the corresponding functions. The message
        payload is supposed to be the keyword arguments in json format.
        '''
        Logger.info('received command (' + str(msg.topic) + "):" + str(msg.payload))
        if msg.topic == 'command/start_game':
            self.game.start_game()
        elif msg.topic == 'command/stop_game':
            self.game.stop_game(force=True)
        elif msg.topic == 'command/force_start_game':
            self.game.force_start_game()
        elif msg.topic == 'command/reset_game':
            self.game.reset_game()
        elif msg.topic == 'command/configure':
            self.call_function_with_payload(self.game.configure, msg.payload)
        elif msg.topic == 'command/set_game_state':
            self.call_function_with_payload(self.game.set_state, msg.payload,
                                            payload_as_kwargs=False)
        elif msg.topic == 'command/set_extra_halftime':
            self.call_function_with_payload(self.game.set_extra_halftime, msg.payload)
        elif msg.topic == 'command/select_game':
            self.call_function_with_payload(self.select_game, msg.payload)
        elif msg.topic == 'command/print':
            self.call_function_with_payload(self.print_protocol, msg.payload)
        elif msg.topic == 'command/add_event':
            self.call_function_with_payload(self.game.add_event, msg.payload)
        elif msg.topic == 'command/delete_event':
            self.call_function_with_payload(self.game.delete_event, msg.payload)
        elif msg.topic == 'command/edit_event':
            self.call_function_with_payload(self.game.edit_event, msg.payload)
        elif msg.topic == 'command/set_linkedtimer_active':
            self.call_function_with_payload(self.game.set_linkedtimer_active, msg.payload)
        elif msg.topic == 'command/answer':
            # Probably send from myself.
            pass
        elif msg.topic == 'command/print_answer':
            # Probably send from myself.
            pass
        elif msg.topic == 'command/reset_all':
            self.reset_all()
        elif msg.topic == 'tournament/gameday':
            self.call_function_with_payload(self.set_gameday, msg.payload,
                                            payload_as_kwargs=False)
        elif msg.topic == 'tournament/lineup':
            self.call_function_with_payload(self.set_lineup, msg.payload,
                                            payload_as_kwargs=False)
        elif msg.topic == 'tournament/roster':
            self.call_function_with_payload(self.set_roster, msg.payload,
                                            payload_as_kwargs=False)
        elif msg.topic == 'tournament/settings':
            self.call_function_with_payload(self.set_tournament_settings, msg.payload,
                                            payload_as_kwargs=False)
        elif msg.topic == 'tournament/delay':
            # Probably send from myself.
            pass
        # elif msg.topic == 'command/horn_request':
        #     self.button_listener.sound_horn()
        elif msg.topic == 'command/horn':
            pass
        else:
            msg = ('received unknown command: '
                   'topic=' + str(msg.topic) + ' '
                   'payload=' + str(msg.payload))
            self.mqtt_publish('command/answer', msg)

    def on_publish(self, client, userdata, mid, reason_codes, properties):
        # Logger.debug(f"finished publishing message {mid} published.")
        pass

    def mqtt_publish(self, topic, payload, qos=1, retain=False, is_error=False):
        with self.mqtt_publish_lock:
            info = None
            try:
                if not self.mqtt_client.is_connected():
                    Logger.debug("Can't publish, because the mqtt client is not connected.\n"
                                 f"topic: {topic}\npayload: {payload}\nqos: {qos}\nretain: {retain}\n")
                    return False
                payload_json = json.dumps(payload)
                if (retain and
                    topic in self.last_published and
                    payload_json == self.last_published[topic]):
                    msg = f"no changes in payload of retained topic '{topic}' -> not published"
                else:
                    info = self.mqtt_client.publish(topic, payload_json, qos, retain)
                    self.last_published[topic] = payload_json
                    msg = (f"added message {info.mid} on {topic} to queue with "
                           f"current result code '{mqtt.error_string(info.rc)}': {payload}")
                if is_error:
                    Logger.error(msg)
                else:
                    Logger.debug(msg)
            except Exception as e:
                msg = ("Something went wrong when trying to publish.\n"
                       f"topic: {topic}\npayload: {payload}\nqos: {qos}\nretain: {retain}\n"
                       f"{e}\n\n{traceback.format_exc()}")
                Logger.error(msg)
            return False

    def call_function_with_payload(self, function, payload, payload_as_kwargs=True,
                                   **extra_kwargs):
        ''' This function is a wrapper to call an other method in this class
        from an mqtt command. This method ensures the any exception of the
        called method will not lead to a crash of the backend. This function
        should be used every time arguments are passed on to the function to
        call form a mqtt command.

        :param callable function: the method to call
        :param json(string) payload: This json string will be deserialized. It
            should be in the from of a dictionary which can be passed as key
            word arguments to the function.
        '''
        # check if json can be deserialized
        try:
            kwargs = json.loads(payload)
            if payload_as_kwargs:
                kwargs.update(extra_kwargs)
        except ValueError as e:
            msg = (
                "ValueError: The payload could not be deserialized. "
                f"function = {function}; payload = {payload}; error msg = {e}\n\n"
                f"{traceback.format_exc()}"
            )
            self.mqtt_publish('command/answer', msg, is_error=True)
            return
        else:
            # try function and catch all exceptions
            try:
                if payload_as_kwargs:
                    function(**kwargs)
                else:
                    function(kwargs)
            except TypeError as e:
                msg = (
                    "TypeError: The keywords probably did not match: "
                    f"function = {function}; payload = {payload}; error msg = {e}\n\n"
                    f"{traceback.format_exc()}"
                )
                self.mqtt_publish('command/answer', msg, is_error=True)
            except ValueError as e:
                msg = (
                    "ValueError: A keyword probably had the wrong value: "
                    f"function = {function}; payload = {payload}; error msg = {e}\n\n"
                    f"{traceback.format_exc()}"
                )
                self.mqtt_publish('command/answer', msg, is_error=True)
            except Exception as e:
                msg = (
                    "Something bad happened. This command could not be executed: "
                    f"function = {function}; payload = {payload}; error msg = {e}\n\n"
                    f"{traceback.format_exc()}"
                )
                self.mqtt_publish('command/answer', msg, is_error=True)

    def set_gameday(self, new_gameday):
        Logger.debug('set new gameday: ' + str(new_gameday))
        game_ids = [int(game['id']) for game in new_gameday]
        if self.game.config['game_id'] not in game_ids + [-1]:
            msg = ('You cannot delete the game that is curently selected in '
                   'the controller.')
            self.mqtt_publish('command/answer', msg)
            self.mqtt_publish('tournament/gameday', self.gameday, retain=True)
            return
        self.gameday = new_gameday
        # delete saved games, that are not in the gameday any more:
        removed_game_ids = [game_id for game_id in self.game.saved_states.keys()
                            if int(game_id) not in game_ids]
        for rgi in removed_game_ids:
            del self.game.saved_states[rgi]
        self.update_plan_game_start_list(self.gameday)
        self.calculate_delay()
        self.save_tournament_to_log()

    def set_lineup(self, new_lineup):
        Logger.debug('set new lineup: ' + str(new_lineup))
        self.lineup = new_lineup
        self.save_tournament_to_log()

    def set_roster(self, new_roster):
        Logger.debug('set new roster: ' + str(new_roster))
        self.roster = new_roster
        self.save_tournament_to_log()

    def set_tournament_settings(self, new_settings):
        Logger.debug('set new tournament settings: ' + str(new_settings))
        self.tournament_settings = new_settings
        hornconfig.tournament_name = self.tournament_settings["name"]
        hornconfig.tournament_town = self.tournament_settings["town"]
        hornconfig.tournament_pool = self.tournament_settings["pool"]
        hornconfig.latexprotocolfile = os.path.join(hornconfig.latexfolder, self.tournament_settings["protocol_file"])
        self.save_tournament_to_log()

    def update_score_gameday(self):
        if self.game.config['game_id'] >= 0:
            current_game = self.find_game_in_gameday(self.game.config['game_id'])
            if current_game is not None:
                new_score = f"{self.game.state['score_blue']} : {self.game.state['score_white']}"
                if not self.game.state['has_started']:
                    new_score = ''
                if not current_game['score'] == new_score:
                    current_game['score'] = new_score
                    self.mqtt_publish('tournament/gameday', self.gameday, retain=True)

    def update_plan_game_start_list(self, new_gameday):
        # create plan_game_start_list dictionary
        plan_game_start_list = []
        for i in range(len(new_gameday)):
            try:
                plan_game_start = dateutil.parser.parse(
                    new_gameday[i]['plan'],
                    ignoretz=False)
            except Exception as e:
                Logger.error(
                    f"could not parse date: {new_gameday[i]['plan']}\n"
                    f"{e}\n\n{traceback.format_exc()}"
                )
            else:
                plan_game_start_list.append(
                    {'id': new_gameday[i]['id'],
                     'plan_game_start': plan_game_start})
        # sort list after game start time (earliest to last game)
        self.plan_game_start_list = sorted(plan_game_start_list,
                                           key=lambda k: k['plan_game_start'])

    def calculate_delay(self):
        # only calculate a new delay list, if a game is selected
        if self.game.config['game_id'] < 0:
            return
        remaining_time = (
            (self.game.config['n_halftime_total'] -
             self.game.state['n_halftime']) *
            (self.game.config['game_length'] +
             self.game.config['halftime_break_length']) +
            self.game.game_timer.get_countdown_time() -
            self.game.halftime_break_timer.get_time())

        datetime_now = datetime.datetime.now(datetime.timezone.utc)
        current_game_end = (datetime_now + datetime.timedelta(seconds=remaining_time))
        min_game_length = (
            (self.game.config['n_halftime_total'] *
             self.game.config['game_length']) +
            ((self.game.config['n_halftime_total'] - 1) *
             self.game.config['halftime_break_length']))
        current_game_found = False
        previous_game_end = current_game_end
        min_break_between_games = hornconfig.min_break_between_games
        delay_dict = {}
        for game in self.plan_game_start_list:
            if game['id'] == self.game.config['game_id']:
                # This is the current game.
                # Delay is not necessary, since the gametime is displayed.
                # The delay is therefore set to 0.
                current_game_found = True
                delay_dict[game['id']] = 0
            elif not current_game_found:
                # This game was planned before the current game.
                # It is assumed that it already happened and the delay is
                # therefore set to 0.
                delay_dict[game['id']] = 0
            else:
                # This is a game after the current game. Delay is necessary.
                # Calculate the delay of this game.
                delay = (
                    previous_game_end +
                    datetime.timedelta(seconds=min_break_between_games) -
                    game['plan_game_start'])
                delay = delay.total_seconds()
                if delay < 0:
                    # No delay.
                    delay = 0
                # Round delay to full minutes
                delay_1min = int(delay / 60.)
                delay = delay_1min * 60
                delay_dict[game['id']] = delay
                # calculate the end of this game, for the
                # delay of the next game.
                plan_game_ends = (
                    game['plan_game_start'] +
                    datetime.timedelta(seconds=min_break_between_games) +
                    datetime.timedelta(seconds=min_game_length))
                previous_game_end = (
                    previous_game_end +
                    datetime.timedelta(seconds=min_break_between_games) +
                    datetime.timedelta(seconds=min_game_length))
                if plan_game_ends > previous_game_end:
                    previous_game_end = plan_game_ends
        self.mqtt_publish('tournament/delay', delay_dict, retain=True)
        return

    def find_game_in_gameday(self, game_id, gameday=None):
        if gameday is None:
            gameday = self.gameday
        current_game = None
        for game in gameday:
            if game_id == game['id']:
                current_game = game
                break
        if current_game is None:
            msg = ('Game with id' + str(game_id) +
                   'was not found in gameday.')
            self.mqtt_publish('command/answer', msg, is_error=True)
        return current_game

    def load_log(self, fname, fname_backup, set_func):
        ''' Loads the old log from ``filename`` and stets this class to it's
        state.

        :param string filename: Path to the log to load.
        '''
        # try to load a logfile
        try:
            with open(fname, "r") as f:
                set_func(json.loads(f.read()))
                Logger.info(f"Successfully loaded file {fname}.")
                return True
        except Exception as e:
            Logger.debug(f"{fname} could not be read, trying backup.")
            try:
                with open(fname_backup, "r") as f:
                    set_func(json.loads(f.read()))
                    Logger.info(f"Successfully loaded file {fname_backup}.")
                    return True
            except Exception as e:
                Logger.error(f"Both {fname} and {fname_backup} could not be read: {e}\n\n{traceback.format_exc()}")
                return False

    def get_tournament_state(self):
        ''' Creates a dictionary with gameday, roster, lineup and game_states.
        This excludes the currently running game.

        :returns: the state of the tournament
        :rtype: dict
        '''
        state = {}
        state['settings'] = self.tournament_settings
        state['gameday'] = self.gameday
        state['roster'] = self.roster
        state['lineup'] = self.lineup
        state['game_states'] = self.game.saved_states
        return state

    def set_tournament_state(self, new_state):
        ''' Sets this class' tournament state. This function needs to be treated with care,
        since no sanity check is performed before applying the new state.

        :param dict new_state: New state of this class.
        '''
        Logger.info('set new tournament state = ' + str(new_state))
        if sorted(self.get_tournament_state().keys()) == sorted(new_state.keys()):
            self.tournament_settings = new_state['settings']
            self.gameday = new_state['gameday']
            self.roster = new_state['roster']
            self.lineup = new_state['lineup']
            self.game.saved_states = new_state['game_states']
        else:
            raise ValueError('this new state has not the correct keys')

    def save_tournament(self, filename):
        ''' Saves the current state of this class in json format.
        '''
        data_string = json.dumps(self.get_tournament_state())
        with open(filename, "w") as f:
            f.write(data_string)
        Logger.debug(f"Saved tournament state to file {filename}.")

    def save_tournament_to_log(self):
        ''' Saves the current state of this class to the log file in json
        format.
        '''
        data_string = json.dumps(self.get_tournament_state())
        if not self.last_saved_tounament_state == data_string:
            with open(hornconfig.tournament_fname, "w") as f:
                f.write(data_string)
            with open(hornconfig.tournament_fname_backup, "w") as f:
                f.write(data_string)
            Logger.debug('saved tournament state.')

    def save_state_to_log_loop(self):
        '''This loop runs in a seperat thread and saves the state alternatingly
        to log and backup log every second. The purpoes of the backup log is
        that at least one file is not corrupted if the power is shut off during
        a write process.
        '''
        is_backup = False
        while self.keep_save_state_to_log_loop_running:
            try:
                state_json = json.dumps(self.game.get_state())
                if is_backup:
                    filename = hornconfig.current_game_fname_backup
                    last_save = 1
                else:
                    filename = hornconfig.current_game_fname
                    last_save = 0
                if self.last_saved_game_state[last_save] != state_json:
                    with open(filename, "w") as f:
                        f.write(state_json)
                    self.last_saved_game_state[last_save] = state_json
            except Exception as e:
                Logger.error(
                    "Something went wrong in saving the current game state: "
                    f"{e}\n\n{traceback.format_exc()}"
                )
            is_backup = not is_backup
            time.sleep(1)

    def publish_time_loop(self):
        ''' This loop runs in seperate thread and calls :meth:`publish_time`
        every 0.1 seconds.
        '''
        Logger.debug('publish time loop has stated')
        while self.keep_publish_time_loop_running:
            try:
                self.game.publish_time()
                self.calculate_delay()
            except Exception as e:
                Logger.error(
                    "Something went wrong in publishing the current time: "
                    f"{e}\n\n{traceback.format_exc()}"
                )
            time.sleep(0.2)

    def print_protocol(self, game_id, mqtt_client_id):
        ''' Prints the protocol for a game.

        :param game_id: game id of the game to print
        :type game_id: int or string
        '''
        Logger.debug('got print command for gameid = ' + str(game_id))
        self.printer.print_protocol(game_id=game_id,
                                    gameday=self.gameday,
                                    roster=self.roster,
                                    lineup=self.lineup,
                                    game_state=self.game.get_state(int(game_id)),
                                    mqtt_client_id=mqtt_client_id)

    def select_game(self, new_id):
        ''' Sets the game Id of the current game. The game Id can only be set,
        if it has not been set before or if the game has not started, jet.
        This was suppsoed to stop confusion during the saving of the score.

        .. todo:: Rethink the not beeing allowd to set the game id during a
            game.

        :param new_id: New game Id
        :type new_id: int or string
        '''
        new_id = int(new_id)
        if new_id == -1:
            # Specifically selectin no game
            blue = 'Blue'
            white = 'White'
        else:
            # let's find the game, which was selected
            current_game = self.find_game_in_gameday(new_id)
            if current_game is None:
                msg = (
                    f"The game id {new_id} was not found in the gameday table. The"
                    " game id -1, which is not linked to any game in the "
                    "gameday, will be used instead."
                )
                self.mqtt_publish('command/answer', msg, is_error=True)
                new_id = -1
                blue = 'Blue'
                white = 'White'
            else:
                blue = current_game['blue']
                white = current_game['white']
                if blue in self.roster:
                    blue = self.roster[blue]['name']
                if white in self.roster:
                    white = self.roster[white]['name']
        self.game.select_game(new_id, blue, white)
        self.calculate_delay()
        self.save_tournament_to_log()

    def sound_end_sequence(self):
        self.button_listener.horn_sequence()


if __name__ == '__main__':
    root_logger = logging.getLogger()
    root_logger.setLevel(logging.INFO)
    log_formatter = logging.Formatter("[%(levelname)s][%(name)s]"
                                      "[%(module)s][%(funcName)s]"
                                      "[%(threadName)s]%(message)s")
    console_handler = logging.StreamHandler()
    console_handler.setFormatter(log_formatter)
    root_logger.addHandler(console_handler)
    root_logger.info('#########  start backend loop #########')
    # start backend
    backend = Backend()
    with backend:
        # run in infitiy loop
        while True:
            time.sleep(1)
