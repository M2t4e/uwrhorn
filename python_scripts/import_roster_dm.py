#!/usr/bin/env python
# -*- coding: utf-8 -*-

from __future__ import print_function
import paho.mqtt.client as mqtt
import json
import os
import csv
import logging
import hornconfig
if not __name__ == '__main__':
    import hornconfig

Logger = logging.getLogger(__name__)

def send_roster(roster):
    client = mqtt.Client(callback_api_version=mqtt.CallbackAPIVersion.VERSION2, client_id="send_roster", clean_session=True)
    client.username_pw_set("GameController", hornconfig.mqtt_password)

    client.connect("localhost",port=8883)
    client.loop_start()
    client.publish('tournament/roster', roster, retain=True)
    Logger.info("Roster sent successfully!")
    client.loop_stop()
    client.disconnect()

def get_csv(directory):
    # pip install --upgrade google-api-python-client google-auth-httplib2 google-auth-oauthlib necessary

    from google.auth.transport.requests import Request
    from google.oauth2.credentials import Credentials
    from google_auth_oauthlib.flow import InstalledAppFlow
    from googleapiclient.discovery import build
    from googleapiclient.errors import HttpError

    # If modifying these scopes, delete the file token.json.
    SCOPES = ['https://www.googleapis.com/auth/spreadsheets']

    # The ID and range of a sample spreadsheet.
    NORD_1_ID = '1I-IiUldTYB3l6i3iv6EDO9fGXzwcMCy_Jv89z6Gkh3c'
    NORD_2_ID = '1DWLYqLpKzf0RTg3qNUwXktwwQw8Hn0KpqcmpvoUKfYM'
    WEST_1_ID = '17yH5OAyyuSy0ckBcPLRWSCBJJWoCA2IAlFYevaE6byY'
    WEST_2_ID = '1JS8H_XFGU9iu85i0JxcqyheG2ew6LqxTKC13XG12Qtc'
    WEST_3_ID = '1wM1aLkjhJLHOvKuIdNd4dzidruZ3ggTywnlj7NNwcGs'
    SUED_1_ID = '14hhho_dgjSD2fNAgLNZ1fvIcxvj8mOzQg-c2XS0R71s'
    SUED_2_ID = '1qzCkRCXYLTx9ZP1_uxsVApNeXUqh6ILqMjz3mJeBHMg'
    SUED_3_ID = '1SRvAV2SDxmfb1VhrztJoeGX00dGbzX8QkfMyFMJvI84'
    TEAM_NAMES_ID = '1Fy0cmwyM4ZEXmNpWJ-kZQd1wl8Z1gG5FBhnq-KIRrng'
    TEMP_ID = 999

    # Destinationfolder for csv files
    PATH = directory

    def delete_temp(service, spreadsheetId):
        if len(service.spreadsheets().get(spreadsheetId=spreadsheetId).execute().get('sheets', '')) > 2:
            requests = [{
                        "deleteSheet": {
                            "sheetId": TEMP_ID
                        }
                    }]

            body = {
                'requests': requests
            }
            service.spreadsheets().batchUpdate(spreadsheetId=spreadsheetId, body=body).execute()

    def copyPasteList(service, SOURCE_SHEET, spreadsheetId):
        requests = [{
                        "addSheet": {
                            "properties": {
                                "title": "temp",
                                "sheetId": TEMP_ID
                            }
                        }
                    },
                    {
                        "copyPaste": {
                            "source": {
                                "sheetId": SOURCE_SHEET,
                                "startRowIndex": 1,
                                "startColumnIndex": 0,
                                "endColumnIndex": 3,
                                "endRowIndex": 19
                            },
                            "destination": {
                                "sheetId": TEMP_ID,
                                "startRowIndex": 1,
                                "startColumnIndex": 0,
                                "endColumnIndex": 3,
                                "endRowIndex": 19
                            },
                            "pasteType": "PASTE_NORMAL",
                            "pasteOrientation": "NORMAL"
                        }
                    }]
        body = {
            'requests': requests
        }
        service.spreadsheets().batchUpdate(spreadsheetId=spreadsheetId, body=body).execute()

    def main():
        """Shows basic usage of the Sheets API.
        Prints values from a sample spreadsheet.
        """
        creds = None
        # The file token.json stores the user's access and refresh tokens, and is
        # created automatically when the authorization flow completes for the first
        # time.
        token_path = os.path.join(os.path.dirname(os.path.realpath(__file__)), 'token.json')
        if os.path.exists(token_path):
            creds = Credentials.from_authorized_user_file(token_path, SCOPES)
        # If there are no (valid) credentials available, let the user log in.
        if not creds or not creds.valid:
            if creds and creds.expired and creds.refresh_token:
                creds.refresh(Request())
            else:
                flow = InstalledAppFlow.from_client_secrets_file(
                    os.path.join(os.path.dirname(os.path.realpath(__file__)), 'credentials.json'), SCOPES)
                creds = flow.run_local_server(port=0)
            # Save the credentials for the next run
            with open(token_path, 'w') as token:
                token.write(creds.to_json())

        try:
            service = build('sheets', 'v4', credentials=creds)
            teamNames = service.spreadsheets().values().get(spreadsheetId=TEAM_NAMES_ID, range="C2:C").execute().get('values', [])
            Logger.info(teamNames)
            participants = service.spreadsheets().values().get(spreadsheetId=TEAM_NAMES_ID, range="D2:D").execute().get('values', [])
            Logger.info(participants)

            for i, team in enumerate(teamNames):
                if participants[i][0] == '1. Platz Liga Nord':
                    spreadsheetId = NORD_1_ID
                    Logger.info("Selecting: " + participants[i][0])
                elif participants[i][0] == '2. Platz Liga Nord':
                    spreadsheetId = NORD_2_ID
                    Logger.info("Selecting: " + participants[i][0])
                elif participants[i][0] == '1. Platz Liga West':
                    spreadsheetId = WEST_1_ID
                    Logger.info("Selecting: " + participants[i][0])
                elif participants[i][0] == '2. Platz Liga West':
                    spreadsheetId = WEST_2_ID
                    Logger.info("Selecting: " + participants[i][0])
                elif participants[i][0] == '3. Platz Liga West':
                    spreadsheetId = WEST_3_ID
                    Logger.info("Selecting: " + participants[i][0])
                elif participants[i][0] == '1. Platz Liga Süd':
                    spreadsheetId = SUED_1_ID
                    Logger.info("Selecting: " + participants[i][0])
                elif participants[i][0] == '2. Platz Liga Süd':
                    spreadsheetId = SUED_2_ID
                    Logger.info("Selecting: " + participants[i][0])
                elif participants[i][0] == '3. Platz Liga Süd':
                    spreadsheetId = SUED_3_ID
                    Logger.info("Selecting: " + participants[i][0])

                delete_temp(service, spreadsheetId)

                SOURCE_SHEET = service.spreadsheets().get(spreadsheetId=spreadsheetId).execute().get('sheets', '')[0].get("properties", {}).get("sheetId", 0)

                copyPasteList(service, SOURCE_SHEET, spreadsheetId)

                values = [
                    [
                        team[0]
                    ],
                ]
                body = {
                    'values': values
                }
                service.spreadsheets().values().update(spreadsheetId=spreadsheetId, range="temp!A1", valueInputOption="RAW", body=body).execute()

                Logger.info("Exporting csv...")
                dataset = service.spreadsheets().values().get(
                    spreadsheetId=spreadsheetId,
                    range="temp!A:C",
                    majorDimension='ROWS'
                ).execute()
                with open(os.path.join(PATH, participants[i][0].lower().replace(' ', '').replace('.platzliga', '').replace('ü', 'ue') + '.csv'), 'w') as file:
                    Logger.info(f"Writing to {file}...")
                    writer = csv.writer(file)
                    writer.writerows(dataset['values'])

                delete_temp(service, spreadsheetId)

        except HttpError as error:
            Logger.info(f"An error occurred: {error}")
            return error

    main()


def import_roster():
    # roster = '{"sporttaucher":{"name":"Sporttaucher","players":{}}}'
    directory = os.path.join(os.path.dirname(os.path.realpath(__file__)), '..', 'team_lists')
    files = []

    if not os.path.exists(directory):
        os.makedirs(directory)

    for file in os.listdir(directory):
        os.remove(directory + '/' + file)

    if len(os.listdir(directory)) == 0:
        Logger.info("Fetching team lists")
        get_csv(directory)

    participantsOrdered = ['1nord',
                           '2nord',
                           '1west',
                           '2west',
                           '3west',
                           '1sued',
                           '2sued',
                           '3sued']
    for filename in participantsOrdered:
        f = os.path.join(directory, filename + '.csv')
        files.append(f)


    roster = {}

    team_placeholders = {
        "1": "SpG Paderborn/Bielefeld",
        "2": "Sporttaucher Berlin I",
        "3": "DUC Krefeld",
        "4": "SV Rheine",
        "5": "Duisburger SCC",
        "6": "TSV Malsch I",
        "7": "TC Bamberg I",
        "8": "DUC Darmstadt",
    }

    for i, f in enumerate(files):
        try:
            with open(f) as file:
                teamname = file.readline().strip().strip(',')
                roster[str(i + 1)] = {"name": teamname,
                                    "players": {}}
                for line in file:
                    if line.strip() == "":
                        continue
                    player = line.strip().split(',')
                    roster[str(i + 1)]["players"][player[0]] = {"name": player[1],
                                                                "passnumber": player[2]}
        except FileNotFoundError:
            Logger.info('No list found for participant ' + f.strip(directory + '/').strip('.csv'))
            roster[str(i + 1)] = {"name": team_placeholders[str(i + 1)], "players": {}}

    dummy_teams = {
        "1A": "1. Platz Gruppe A",
        "2A": "2. Platz Gruppe A",
        "3A": "3. Platz Gruppe A",
        "4A": "4. Platz Gruppe A",
        "1B": "1. Platz Gruppe B",
        "2B": "2. Platz Gruppe B",
        "3B": "3. Platz Gruppe B",
        "4B": "4. Platz Gruppe B",
        "Z1G": "Gewinner Spiel 13",
        "Z1V": "Verlierer Spiel 13",
        "Z2G": "Gewinner Spiel 14",
        "Z2V": "Verlierer Spiel 14",
        "Z3G": "Gewinner Spiel 15",
        "Z3V": "Verlierer Spiel 15",
        "Z4G": "Gewinner Spiel 16",
        "Z4V": "Verlierer Spiel 16",
        "REF1":	"Manfred Kazur",
        "REF2":	"Ulli Wagenhäuser",
        "REF3":	"Markus Heckrath",
        "REF4":	"Herbi Kindermann",
        "REF5":	"Daniel Rieder",
        "REF6":	"Tim Sainsbury",
        "REF7":	"Helmut Poguntke",
        "REF8":	"Bernd Reichhüber",
        "REF9":	"Timo Frank",
        "REF10": "Jan Cohrs",
        "REF11": "Birgit Lüdke",
        "REF12": "Bob Robinson"
    }
    for name_id, name in dummy_teams.items():
        roster[name_id] = {"name": name, "players": {}}

    return roster


if __name__ == '__main__':
    root_logger = logging.getLogger()
    root_logger.setLevel(logging.INFO)
    log_formatter = logging.Formatter("[%(levelname)s][%(name)s]"
                                      "[%(module)s][%(funcName)s]"
                                      "[%(threadName)s]%(message)s")
    console_handler = logging.StreamHandler()
    console_handler.setFormatter(log_formatter)
    root_logger.addHandler(console_handler)

    roster = import_roster()
    send_roster(json.dumps(roster))
