#!/usr/bin/env python
# -*- coding: utf-8 -*-

import re
import os
import shutil
import logging
import subprocess
import dateutil.parser
import datetime
import hornconfig
import threading
import traceback


Logger = logging.getLogger(__name__)


class Printer():

    def __init__(self, mqtt_publish):
        self.mqtt_publish = mqtt_publish
        self.lock = threading.Lock()

    def print_protocol(self, **kwargs):
        try:
            t = threading.Thread(target=self._print_protocol,
                                 kwargs=kwargs)
            t.daemon = True
            t.start()
        except Exception as e:
            Logger.error('got exception in printing thread. '
                         'Exception msg = ' + str(e) + '\n\n' +
                         traceback.format_exc())

    def _print_protocol(self, game_id, gameday, roster, lineup, game_state, mqtt_client_id):
        with self.lock:
            game_id = str(game_id)
            Logger.debug('getting the relevant data for lineup:' +
                         str(lineup))
            # get row id
            row_id = -1
            Logger.debug(gameday)
            for i in range(len(gameday)):
                if str(gameday[i]['id']) == game_id:
                    row_id = i
                    break
            if row_id == -1:
                Logger.warning('could not find a game id: {}'.format(game_id))
                return
            Logger.debug('found row_id = ' + str(row_id))
            teamblue = gameday[row_id]['blue']
            teamwhite = gameday[row_id]['white']
            gender = gameday[row_id]['gender_category']
            main_ref = gameday[row_id]['main_ref']
            uw_ref_1 = gameday[row_id]['uw_ref_1']
            uw_ref_2 = gameday[row_id]['uw_ref_2']
            gamename = hornconfig.tournament_name
            try:
                gamedate = dateutil.parser.parse(gameday[row_id]['plan']).astimezone(datetime.timezone(datetime.timedelta(hours=2)))
                gamedate = gamedate.strftime('%d.%m.%Y')
            except Exception as e:
                Logger.exception('Could not parse gamedate. ' + str(e))
                gamedate = ''
            gametown = hornconfig.tournament_town
            gamepool = hornconfig.tournament_pool
            lineup_blue = {}
            leaderblue = ''
            captainblue = ''
            lineup_white = {}
            leaderwhite = ''
            captainwhite = ''
            Logger.debug(str(lineup))
            if game_id in lineup:
                if teamblue in lineup[game_id]:
                    if u'member' in lineup[game_id][teamblue]:
                        lineup_blue = lineup[game_id][teamblue]['member']
                    if u'leader' in lineup[game_id][teamblue]:
                        leaderblue = lineup[game_id][teamblue]['leader']
                    if u'starting_lineup' in lineup and game_id in lineup['starting_lineup'] and teamblue in lineup['starting_lineup'][game_id]:
                        if u'captain' in lineup['starting_lineup'][game_id][teamblue]:
                            captainblue = lineup['starting_lineup'][game_id][teamblue]['captain']
                    else:
                        if u'captain' in lineup[game_id][teamblue]:
                            captainblue = lineup[game_id][teamblue]['captain']
                else:
                    Logger.warning(f'could not find {teamblue} in lineup at game_id {game_id}')
                if teamwhite in lineup[game_id]:
                    if u'member' in lineup[game_id][teamwhite]:
                        lineup_white = lineup[game_id][teamwhite]['member']
                    if u'leader' in lineup[game_id][teamwhite]:
                        leaderwhite = lineup[game_id][teamwhite]['leader']
                    if u'starting_lineup' in lineup and game_id in lineup['starting_lineup'] and teamwhite in lineup['starting_lineup'][game_id]:
                        if u'captain' in lineup['starting_lineup'][game_id][teamwhite]:
                            captainwhite = lineup['starting_lineup'][game_id][teamwhite]['captain']
                    else:
                        if u'captain' in lineup[game_id][teamwhite]:
                            captainwhite = lineup[game_id][teamwhite]['captain']
                else:
                    Logger.warning(f'could not find {teamwhite} in lineup at gmae_id {game_id}')
            else:
                Logger.warning(f'could not find game_id: {game_id} in lineup.')
            Logger.debug(str(roster))
            players_blue = {}
            players_white = {}
            teamblue_name = teamblue
            teamwhite_name = teamwhite
            nation_blue = ''
            nation_white = ''
            if teamblue in roster:
                players_blue = roster[teamblue]['players']
                teamblue_name = roster[teamblue]['name']
                if "nation" in roster[teamblue]:
                    nation_blue = roster[teamblue]['nation']
                else:
                    nation_blue = ''
            else:
                Logger.warning(f'could not find {teamblue} in roster.')
            if teamwhite in roster:
                players_white = roster[teamwhite]['players']
                teamwhite_name = roster[teamwhite]['name']
                if "nation" in roster[teamwhite]:
                    nation_white = roster[teamwhite]['nation']
                else:
                    nation_white = ''
            else:
                Logger.warning(f'could not find {teamwhite} in roster.')
            player_lines = self.create_player_lines(
                lineup_blue,
                lineup_white,
                players_blue,
                players_white)

            if main_ref in roster:
                main_ref = roster[main_ref]['name'].upper() + " (" + roster[main_ref]['nation'] + ")"
            else:
                Logger.warning(f'could not find {main_ref} in roster.')
            if uw_ref_1 in roster:
                uw_ref_1 = roster[uw_ref_1]['name'].upper() + " (" + roster[uw_ref_1]['nation'] + ")"
            else:
                Logger.warning(f'could not find {uw_ref_1} in roster.')
            if uw_ref_2 in roster:
                uw_ref_2 = roster[uw_ref_2]['name'].upper() + " (" + roster[uw_ref_2]['nation'] + ")"
            else:
                Logger.warning(f'could not find {uw_ref_2} in roster.')

            if game_state is not None:
                try:
                    starttime = dateutil.parser.parse(game_state['state']['start_time']).astimezone(datetime.timezone(datetime.timedelta(hours=2))).strftime('%H:%M')
                except Exception as e:
                    Logger.exception('Could not parse starttime. ' + str(e))
                    starttime = ''
                try:
                    endtime = dateutil.parser.parse(game_state['state']['end_time']).astimezone(datetime.timezone(datetime.timedelta(hours=2))).strftime('%H:%M')
                except Exception as e:
                    Logger.exception('Could not parse endtime. ' + str(e))
                    endtime = ''
                scoreblue = str(game_state['state']['score_blue'])
                scorewhite = str(game_state['state']['score_white'])
                protocol_lines = self.create_protocol_lines(game_state['protocol'])
            else:
                starttime = ''
                endtime = ''
                scoreblue = '\hspace{1.5em}'
                scorewhite = '\hspace{1.5em}'
                protocol_lines = self.create_protocol_lines([])

            if gender == "f":
                gender_string = '\\manfalse\n\\womantrue\n'
            else:
                gender_string = '\\mantrue\n\\womanfalse\n'
            
            nationString_blue = nation_blue
            nationString_white = nation_white
            if nationString_blue != '':
                nationString_blue = " (" + nationString_blue + ")"
            if nationString_white != '':
                nationString_white = " (" + nationString_white + ")"
            

            data = (f'\\newcommand{{\\teamblue}}{{{teamblue_name + nationString_blue}}}\n'
                    f'\\newcommand{{\\teamwhite}}{{{teamwhite_name + nationString_white}}}\n'
                    f'\\newcommand{{\\gamedate}}{{{gamedate}}}\n'
                    f'\\newcommand{{\\gamename}}{{{gamename}}}\n'
                    f'\\newcommand{{\\gametown}}{{{gametown}}}\n'
                    f'\\newcommand{{\\gamepool}}{{{gamepool}}}\n'
                    f'\\newcommand{{\\playerlines}}{{\n{player_lines}}}\n'
                    f'\\newcommand{{\\protocollines}}{{\n{protocol_lines}}}\n'
                    f'\\newcommand{{\\leaderblue}}{{{leaderblue}}}\n'
                    f'\\newcommand{{\\leaderwhite}}{{{leaderwhite}}}\n'
                    f'\\newcommand{{\\captainblue}}{{{captainblue}}}\n'
                    f'\\newcommand{{\\captainwhite}}{{{captainwhite}}}\n'
                    f'\\newcommand{{\\uwrefone}}{{{uw_ref_1}}}\n'
                    f'\\newcommand{{\\uwreftwo}}{{{uw_ref_2}}}\n'
                    f'\\newcommand{{\\deckref}}{{{main_ref}}}\n'                    
                    f'\\newcommand{{\\scoreblue}}{{{scoreblue}}}\n'
                    f'\\newcommand{{\\scorewhite}}{{{scorewhite}}}\n'
                    f'\\newcommand{{\\gamestarttime}}{{{starttime}}}\n'
                    f'\\newcommand{{\\gameendtime}}{{{endtime}}}\n'
                    f'\\newcommand{{\\gamenumber}}{{{game_id}}}\n'
                    f'{gender_string}'
                    '\\juniorfalse\n'
                    '\\seniorfalse\n')
            Logger.debug('generated the following data:' + data)
            Logger.debug(f'writing data to file: {hornconfig.latexdatafile}')
            with open(hornconfig.latexdatafile, 'w') as f:
                f.write(data)

            folder = hornconfig.protocolfolder
            if not os.path.isdir(folder):
                os.makedirs(folder)

            filename = ('protocol_' + gamedate + '_' +
                        teamblue + '_' + teamwhite + '_')
            filename = re.sub(r'[^\w\-_\.]', '_', filename)
            filename = os.path.join(folder, filename)
            i = 0
            while os.path.isfile(filename + str(i) + '.pdf'):
                i = i + 1
            filename = filename + str(i) + '.pdf'

            Logger.debug('runnign pdflatex -> generating pdf')
            cwd = os.getcwd()
            os.chdir(hornconfig.latexfolder)
            pdflatex = subprocess.run(['pdflatex',
                                       '-interaction=nonstopmode',
                                       hornconfig.latexprotocolfile],
                                      stdout=subprocess.PIPE)
            os.chdir(cwd)

            pdfprotocolfile = os.path.splitext(hornconfig.latexprotocolfile)[0] + '.pdf'
            outfile = os.path.join(hornconfig.latexfolder, pdfprotocolfile)

            Logger.debug('copying finished protocol to ' + filename)
            shutil.copy(outfile, filename)

        msg = {'mqtt_client_id': mqtt_client_id,
               'url': '/protocols/' + os.path.basename(filename),
               'filename': os.path.basename(filename)}
        self.mqtt_publish('command/print_answer', msg)

    def create_player_lines(self, lineup_blue, lineup_white,
                            roster_blue, roster_white):
        water_blue, sub_blue, exchanged_blue = self.sort_players(lineup_blue)
        water_white, sub_white, exchanged_white = self.sort_players(lineup_white)

        lines = ""
        for i in range(0, 15):
            if len(water_blue) > i:
                lines += self.create_player_linesection(
                    water_blue[i],
                    True,
                    roster_blue)
            elif len(exchanged_blue) > i - len(water_blue):
                lines += self.create_player_linesection(
                    exchanged_blue[i - len(water_blue)],
                    False,
                    roster_blue,
                    is_exchanged=True)
            elif len(sub_blue) > i - len(water_blue) - len(exchanged_blue):
                lines += self.create_player_linesection(
                    sub_blue[i - len(water_blue) - len(exchanged_blue)],
                    False,
                    roster_blue)
            else:
                lines += '& & & '

            lines += '& '

            if len(water_white) > i:
                lines += self.create_player_linesection(
                    water_white[i],
                    True,
                    roster_white)
            elif len(exchanged_white) > i - len(water_white):
                lines += self.create_player_linesection(
                    exchanged_white[i - len(water_white)],
                    False,
                    roster_white,
                    is_exchanged=True)
            elif len(sub_white) > i - len(water_white) - len(exchanged_white):
                lines += self.create_player_linesection(
                    sub_white[i - len(water_white) - len(exchanged_white)],
                    False,
                    roster_white)
            else:
                lines += '& & & '

            lines += '\\\\ \\hline\n'

        return lines

    def create_player_linesection(self, number, is_water, roster, is_exchanged=False):
        name = ''
        passnumber = ''
        Logger.debug(str(roster))
        Logger.debug(str(number))
        Logger.debug(str(number in roster))
        if number in roster:
            if 'name' in roster[number]:
                name = roster[number]['name']
            if 'passnumber' in roster[number]:
                passnumber = roster[number]['passnumber']

        section = (str(number) + ' & ' +
                   name + ' & ' +
                   str(passnumber) + ' & ')

        if is_water:
            section += 'x '
        elif is_exchanged:
            section += '\ooalign{$\\bigcirc$\cr\hss x\hss} '

        return section

    def sort_players(self, lineup):
        water = []
        sub = []
        exchanged = []
        for (number, status) in lineup.items():
            if status == 'water':
                water.append(int(number))
            elif status == 'sub':
                sub.append(int(number))
            elif status == 'exchanged':
                exchanged.append(int(number))
        water.sort()
        sub.sort()
        exchanged.sort()
        water = list(map(str, water))
        sub = list(map(str, sub))
        exchanged = list(map(str, exchanged))
        return water, sub, exchanged

    def create_protocol_lines(self, protocol):
        lines = ''
        count = 0
        for game_stop in protocol:
            for event in game_stop['events']:
                new_line = game_stop['gametime_text'] + ' & '
                if event['teamcolor'] == 'blue':
                    new_line += 'B & '
                elif event['teamcolor'] == 'white':
                    new_line += 'W & '
                else:
                    new_line += '& '
                new_line += str(event['player_nr']) + ' & '
                if event['event_type'] == 'goal':
                    new_line += r'$\boxtimes$ & $\Box$ & $\Box$ & $\Box$ & $\Box$ & $\Box$ & $\Box$ & $\Box$ & $\Box$ & $\Box$ & '
                elif event['event_type'] in ['warning', 'teamwarning']:
                    new_line += r'$\Box$ & $\boxtimes$ & $\Box$ & $\Box$ & $\Box$ & $\Box$ & $\Box$ & $\Box$ & $\Box$ & $\Box$ & '
                elif event['event_type'] in ['penaltytime', 'doublepenaltytime']:
                    new_line += r'$\Box$ & $\Box$ & $\boxtimes$ & $\Box$ & $\Box$ & $\Box$ & $\Box$ & $\Box$ & $\Box$ & $\Box$ & '
                elif event['event_type'] == 'matchpenalty':
                    new_line += r'$\Box$ & $\Box$ & $\Box$ & $\boxtimes$ & $\Box$ & $\Box$ & $\Box$ & $\Box$ & $\Box$ & $\Box$ & '
                elif event['event_type'] == 'playerexchange':
                    new_line += r'$\Box$ & $\Box$ & $\Box$ & $\Box$ & $\boxtimes$ & $\Box$ & $\Box$ & $\Box$ & $\Box$ & $\Box$ & '
                elif event['event_type'] == 'timeout':
                    new_line += r'$\Box$ & $\Box$ & $\Box$ & $\Box$ & $\Box$ & $\boxtimes$ & $\Box$ & $\Box$ & $\Box$ & $\Box$ & '
                elif event['event_type'] == 'freethrow':
                    new_line += r'$\Box$ & $\Box$ & $\Box$ & $\Box$ & $\Box$ & $\Box$ & $\boxtimes$ & $\Box$ & $\Box$ & $\Box$ & '
                elif event['event_type'] == 'penaltythrow':
                    new_line += r'$\Box$ & $\Box$ & $\Box$ & $\Box$ & $\Box$ & $\Box$ & $\Box$ & $\boxtimes$ & $\Box$ & $\Box$ & '
                elif event['event_type'] == 'refereeball':
                    new_line += r'$\Box$ & $\Box$ & $\Box$ & $\Box$ & $\Box$ & $\Box$ & $\Box$ & $\Box$ & $\boxtimes$ & $\Box$ & '
                elif event['event_type'] == 'teamball':
                    new_line += r'$\Box$ & $\Box$ & $\Box$ & $\Box$ & $\Box$ & $\Box$ & $\Box$ & $\Box$ & $\Box$ & $\boxtimes$ & '
                else:
                    new_line += r'$\Box$ & $\Box$ & $\Box$ & $\Box$ & $\Box$ & $\Box$ & $\Box$ & $\Box$ & $\Box$ & $\Box$ & '
                if event['event_type'] == 'doublepenaltytime':
                    new_line += 'Double Penalty Time; '
                elif event['event_type'] == 'halftimebreak':
                    new_line += 'End of Half Nr. ' + str(game_stop['halftime']) + '; '
                elif event['event_type'] == 'teamwarning':
                    new_line += 'Team warning; '
                new_line += event['comment'] + ' & '
                if event['event_type'] == 'goal':
                    score = event['extra'].split(':')
                    new_line += score[0] + ' & ' + score[1] + r' \\ \hline'
                else:
                    new_line += r'& \\ \hline'
                lines += new_line + '\n'
                count += 1
        emptylines = 40
        if "protocol_cc.tex" in hornconfig.latexprotocolfile:
            emptylines = 4
        if count < emptylines:
            lines += ''.join([r'& & & $\Box$ & $\Box$ & $\Box$ & $\Box$ & $\Box$ & $\Box$ & $\Box$ & $\Box$ & $\Box$ & $\Box$ &  & & \\ \hline' + '\n']*(emptylines-count))
        return lines
