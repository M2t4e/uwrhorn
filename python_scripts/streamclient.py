#!/usr/bin/env python
# -*- coding: utf-8 -*-

import time
import json
import traceback
import subprocess
import threading
import paho.mqtt.client as mqtt

# this is only needed to put the path name together
import os

IS_EMULATOR = False
DELAY_TIME = 3.0

if IS_EMULATOR:
    import numpy as np

class StreamMqttClient():

    def __init__(self):
        self.client = mqtt.Client(clean_session=True)
        self.client.on_connect = self.on_connect
        self.client.on_message = self.on_message

        self.score_blue = 0
        self.score_white = 0
        self.teamname_blue = ''
        self.teamname_white = ''
        self.gametime = ''
        self.script_lock = threading.Lock()
        self.timefile_lock = threading.Lock()
        self.delay_lock = threading.Lock()
        self.game_state = None

        self.last_time_string = ''
        self.old_command_list = []

    def __enter__(self):
        self.client.loop_start()
        self.client.connect_async('uwr.local', 1883, 60)
        return self

    def __exit__(self, type, value, traceback):
        self.client.loop_stop()
        self.client.disconnect()

    def on_connect(self, client, userdata, flags, rc):
        print("Connected with result code " + str(rc))
        self.client.subscribe("state/#", 1)

    def on_message(self, *args, **kwargs):
        t = threading.Timer(DELAY_TIME, self._on_message,
                            args, kwargs)
        t.start()

    def _on_message(self, client, userdata, msg):
        with self.delay_lock:
            print("New message: topic=" + str(msg.topic) + " payload=" +
                  str(msg.payload))
            update_game_state = False
            try:
                if str(msg.topic) == 'state/current_time':
                    current_time = json.loads(msg.payload)
                    self.gametime = current_time['down']
                    ###########################################################
                    # write in here what you want in the file for the time
                    time_string = ('gametime=' + str(self.gametime))
                    if not time_string == self.last_time_string:
                        self.last_time_string = time_string
                        # write string to file
                        # filename = os.path.join('path', 'to', 'file',
                        #                         'time.txt')
                        filename = 'time.txt'
                        #######################################################
                        if self.timefile_lock.acquire(False):
                            t = threading.Thread(target=self.printing_file,
                                                 args=[filename, time_string])
                            t.start()
                            print('printing filename')
                elif str(msg.topic) == 'state/game_state':
                    self.game_state = json.loads(msg.payload)
                    update_game_state = True
                elif str(msg.topic) == 'state/score_blue':
                    self.score_blue = int(msg.payload)
                    update_game_state = True
                elif str(msg.topic) == 'state/score_white':
                    self.score_white = int(msg.payload)
                    update_game_state = True
                elif str(msg.topic) == 'state/teamname_blue':
                    self.teamname_blue = str(msg.payload)
                    update_game_state = True
                elif str(msg.topic) == 'state/teamname_white':
                    self.teamname_white = str(msg.payload)
                    update_game_state = True
                else:
                    return
            except Exception as e:
                msg = ('The message caused an error:\n'
                       'topic = ' + str(msg.topic) + '\n'
                       'payload = ' + str(msg.payload) + '\n'
                       'error msg = ' + str(e) + '\n' +
                       traceback.format_exc())
                print(msg)
                return
            if update_game_state:
                try:
                    threading.Thread(target=self.starting_script).start()
                except Exception as e:
                    msg = ('Error calling the script:\n'
                           'error msg = ' + str(e) + '\n' +
                           traceback.format_exc())
                    print(msg)

    def starting_script(self):
        with self.script_lock:
            try:
                ###########################################################
                # write in here what you need for the game state
                statustext = ''
                if self.game_state['is_timeout']:
                    statustext = 'Auszeit'
                elif self.game_state['is_penaltythrow']:
                    statustext = 'Strafwurf'
                elif self.game_state['is_halftime_break']:
                    statustext = 'Halbzeitpause'
                elif self.game_state['is_match_over']:
                    statustext = 'Beended'
                else:
                    statustext = (str(self.game_state['n_halftime']) +
                                  '. Halbzeit')

                # subprocess.call(['all', 'the', 'commands', 'needed',
                #                  'to', 'start', 'your', 'script'])
                # for example:

                command_list = ['./test.sh',
                                "'" + statustext + "'",
                                "'" + str(self.teamname_blue) + "'",
                                "'" + str(self.teamname_white) + "'",
                                "'" + str(self.score_blue) + "'",
                                "'" + str(self.score_white) + "'"]
                if not command_list == self.old_command_list:
                    self.old_command_list = command_list
                    subprocess.call(command_list)
                ###########################################################
            except Exception as e:
                msg = ('Error calling the script:\n'
                       'error msg = ' + str(e) + '\n' +
                       traceback.format_exc())
                print(msg)

    def printing_file(self, filename, text):
        try:
            with open(filename, 'w') as f:
                f.write(text)
        except Exception as e:
            msg = ('Error wrinting the file:\n'
                   'error msg = ' + str(e) + '\n' +
                   traceback.format_exc())
            print(msg)
        finally:
            self.timefile_lock.release()


def emulator_thread(smc):
    msg = mqtt.MQTTMessage()
    msg.topic = 'state/teamname_blue'
    msg.payload = 'Rheine'
    smc.on_message(None, None, msg)
    msg = mqtt.MQTTMessage()
    msg.topic = 'state/teamname_white'
    msg.payload = 'Hamburg'
    smc.on_message(None, None, msg)
    count = 0
    halftime = 0
    score_blue = 0
    score_white = 0
    while True:
        countdown = 900 - count
        m, s = (np.floor(divmod(count, 60)))
        up = '%02d:%02d' % (m, s)
        m, s = (np.ceil(divmod(countdown, 60)))
        down = '%02d:%02d' % (m, s)
        msg = mqtt.MQTTMessage()
        msg.topic = 'state/current_time'
        msg.payload = ('{"down": "' + down + '", '
                       '"real": "2017-05-11T00:22:56.058549", '
                       '"up": "' + up + '"}')
        smc.on_message(None, None, msg)
        if (count % 60) == 0:
            halftime = halftime + 1
            msg = mqtt.MQTTMessage()
            msg.topic = 'state/game_state'
            msg.payload = ('{"is_halftime_break": false, '
                           '"start_time": "2017-05-11T00:22:29.811534", '
                           '"is_gametime_running": true, '
                           '"has_started": true, '
                           '"is_timeout": false, '
                           '"end_time": "", '
                           '"game_id": -1, '
                           '"n_halftime": ' + str(halftime) + ', '
                           '"is_penaltythrow": false, '
                           '"is_match_over": false}')
            smc.on_message(None, None, msg)
        if (count % 20) == 0:
            score_blue = score_blue + 1
            msg = mqtt.MQTTMessage()
            msg.topic = 'state/score_blue'
            msg.payload = str(score_blue)
            smc.on_message(None, None, msg)
        if (count % 20) == 10:
            score_white = score_white + 1
            msg = mqtt.MQTTMessage()
            msg.topic = 'state/score_white'
            msg.payload = str(score_white)
            smc.on_message(None, None, msg)
        count = count + 1
        count = count % 900
        time.sleep(1)


if __name__ == '__main__':
    smc = StreamMqttClient()
    with smc:
        if IS_EMULATOR:
            t = threading.Thread(target=emulator_thread,
                                 args=[smc])
            t.daemon = True
            t.start()
        while True:
            time.sleep(1)
