#!/usr/bin/env python
# -*- coding: utf-8 -*-

'''
The module hornbuttons handels the in and output via the Raspberry Pi's general
purpose input output pins (GPIO) as well es the detection of sequences of
button actuation.
'''

import threading
import logging
import horntimers
import gpiozero
import traceback
from threading import current_thread
from time import sleep

# These two lines are for testing the script on non RPi devices.
# from gpiozero.pins.mock import MockFactory
# gpiozero.Device.pin_factory = MockFactory()

Logger = logging.getLogger(__name__)

class ButtonListener (object):
    ''' This class handels the input interrupts and sets the output for the
    horn. It should only be instantiated once, since the pins are hardcoded.'''

    def __init__(self, on_sequence, off_sequence, sequence_detection_time, mqtt_publish):
        '''
        :param callable on_sequence: Callback function when a sequence is
            deteced on any input.
        :param callable off_sequence: Callback function when a single press is
            detected on main referee input.
        :param int sequence_detection_time: Time span between releasing and
            pressing a button to still count as one sequence in seconds.
        :param callable extra_button: Callback function when the exta button is
            pressed (e.g. for restarting the program)
        '''
        Logger.info('initialise ButtonListener object ' + str(id(self)))
        self.active_buttons = set()
        self.is_blocked = False

        self.horn_pin = 4
        self.main_referee_pin = 17
        self.uw_referee_1_pin = 27
        self.uw_referee_2_pin = 22

        self.mqtt_publish = mqtt_publish

        self.main_referee_pin = RefereeButton(pin=self.main_referee_pin,
                                              sequence_detection_time=sequence_detection_time,
                                              mqtt_publish=self.mqtt_publish,
                                              on_sequence=on_sequence,
                                              off_sequence=off_sequence)
        self.uw_referee_1_pin = RefereeButton(pin=self.uw_referee_1_pin,
                                              sequence_detection_time=sequence_detection_time,
                                              mqtt_publish=self.mqtt_publish,
                                              on_sequence=on_sequence,
                                              off_sequence=None)
        self.uw_referee_2_pin = RefereeButton(pin=self.uw_referee_2_pin,
                                              sequence_detection_time=sequence_detection_time,
                                              mqtt_publish=self.mqtt_publish,
                                              on_sequence=on_sequence,
                                              off_sequence=None)
        self.horn = HornBuzzer(
            self.mqtt_publish_horn_on,
            self.mqtt_publish_horn_off,
            self.horn_pin)

    def horn_sequence(self, on_time=1.0, off_time=0.3, n=5):
        '''Make the device turn on and off repeatedly.

        :param float on_time:
            Number of seconds on. Defaults to 1 second.
        :param float off_time:
            Number of seconds off. Defaults to 0.3 second.
        :type n: int or None
        :param n:
            Number of times to honk; `None` means forever. Defaults to 5 times.
        '''
        self.horn.beep(on_time, off_time, n, background=True)
    
    def mqtt_publish_horn_on(self):
        self.mqtt_publish("command/horn", "horn_on")

    def mqtt_publish_horn_off(self):
        self.mqtt_publish('command/horn', "horn_off")


###############################################################################
class RefereeButton(object):
    '''Checks if a button actuation belongs to a actuation sequence ot this
    button.
    '''

    def __init__(self, pin, sequence_detection_time, mqtt_publish,
                 on_sequence=None, off_sequence=None):
        '''
        :param int pin: GPIO pin number corresponding to this button.
        :param callable pressed: Function to call if button is pressed
        :param callable released: Function to call if button is released.
        :param int sequence_detection_time: Time span between releasing
            and pressing a button to still count as one sequence.
        :param callable on_sequence: Function to call when a sequence is
            deteced this input.
        :param callable off_sequence: Function to all when a single press is
            detected on this input.
        '''
        self._sequence = False
        self._new_sequence = True

        self.button_lock = threading.Lock()

        self.mqtt_publish = mqtt_publish
        self.on_sequence = on_sequence
        self.off_sequence = off_sequence
        self.timer = horntimers.Timer(
            sequence_detection_time, self._end_sequence)

        self.button = gpiozero.Button(pin, pull_up=False)
        self.button.when_pressed = self.button_pressed
        self.button.when_released = self.button_released

    def button_pressed(self):
        ''' Called when the button is pressed.
        '''
        with self.button_lock:
            try:
                Logger.debug('buton pressed: ' + str(self.button))
                if self._sequence:
                    if self._new_sequence:
                        if callable(self.on_sequence):
                            self.on_sequence()
                        self._new_sequence = False
                else:
                    if callable(self.off_sequence):
                        self.off_sequence()
                self.timer.stop()  # stop timer
                self.mqtt_publish('command/horn', "horn_on")
            except Exception as e:
                msg = ('Something went wrong when the button {} was pressed.\n'
                       ''.format(self.button.pin) + str(e) + '\n\n' +
                       traceback.format_exc())
                self.mqtt_publish('command/answer', msg, is_error=True)

    def button_released(self):
        ''' Called when the button is released. It continues or starts a
        sequence of button acutations.
        '''
        with self.button_lock:
            try:
                Logger.debug('button released: ' + str(self.button))
                self.timer.restart()
                self._sequence = True
                self.mqtt_publish('command/horn', "horn_off")
            except Exception as e:
                msg = ('Something went wrong when the button {} was released.\n'
                       ''.format(self.button.pin) + str(e) + '\n\n' +
                       traceback.format_exc())
                self.mqtt_publish('command/answer', msg, is_error=True)

    def _end_sequence(self):
        ''' Called when internal timer has run out and indicates that
        the sequence is over.'''
        with self.button_lock:
            self._sequence = False
            self._new_sequence = True


class HornBuzzer(gpiozero.Buzzer):
    def __init__(self, buzzer_on_function, buzzer_off_function, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.buzzer_on_function = buzzer_on_function
        self.buzzer_off_function = buzzer_off_function

    def _write(self, value):
        super()._write(value)
        if value:
            self.buzzer_on_function()
        else:
            self.buzzer_off_function()
