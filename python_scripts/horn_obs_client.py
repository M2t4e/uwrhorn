#!/usr/bin/env python

import obspython as obs
import time
import json
import traceback
import subprocess
import threading
import paho.mqtt.client as mqtt

enabled = False
debug_mode = True
nubber_of_fileds = 3
display_text = [''] * nubber_of_fileds
source_name = [''] * nubber_of_fileds
old_outputtext = [''] * nubber_of_fileds
client = None

timetext = ''
statustext = ''
game_state = {}
score_blue = '0'
score_white = '0'
teamname_blue = ''
teamname_white = ''
n_halftime = '0'

# ------------------------------------------------------------
# OBS Script Functions
# ------------------------------------------------------------

def script_defaults(settings):
    global debug_mode
    if debug_mode: print("[HORN] Calling defaults")

    global enabled
    global source_name
    global display_text

    obs.obs_data_set_default_bool(settings, "enabled", enabled)
    for i in range(len(display_text)):
        obs.obs_data_set_default_string(settings, "source_name_" + str(i), source_name[i])
        obs.obs_data_set_default_string(settings, "display_text_" + str(i), display_text[i])
	
def script_description():
    global debug_mode
    if debug_mode: print("[HORN] Calling description")

    return "<b>Horn OBS client</b>" + \
	    "<hr>" + \
	    "Display the time and score of the current game on your screen." + \
	    "<br/><br/>" + \
	    "Available placeholders: " + \
	    "<br/>" + \
	    "<code>%time</code>, <code>%status</code>, <code>%teamname_blue</code>, <code>%teamname_white</code>, <code>%score_blue</code>, <code>%score_blue</code>, <code>%n_halfetime</code>" + \
	    "<hr>"

def script_load(settings):
    global debug_mode
    if debug_mode: print("[HORN] Loaded script.")

    global client
    # Here create the mqtt client
    client = mqtt.Client('videaostream', clean_session=True)
    client.on_connect = on_connect
    client.on_message = on_message
    enabled = False
	
def script_properties():
    global debug_mode
    if debug_mode: print("[HORN] Loaded properties.")

    props = obs.obs_properties_create()
    obs.obs_properties_add_bool(props, "debug_mode", "Debug Mode")
    obs.obs_properties_add_bool(props, "enabled", "Enabled")
    for i in range(len(display_text)):
        obs.obs_properties_add_text(props,
                                    "source_name_" + str(i),
                                    "Text source " + str(i),
                                    obs.OBS_TEXT_DEFAULT )
        obs.obs_properties_add_text(props,
                                    "display_text_" + str(i),
                                    "Display text " + str(i),
                                    obs.OBS_TEXT_DEFAULT )
    return props

def script_save(settings):
    global debug_mode
    if debug_mode: print("[HORN] Saved properties.")
    script_update(settings)

def script_unload():
    global debug_mode
    if debug_mode: print("[HORN] Unloaded script.")
    global client
    # Here destroy mqtt client
    if client is not None:
        client.loop_stop()
        client.disconnect()
        client = None
    enabled = False

def script_update(settings):
    global debug_mode
    if debug_mode: print("[HORN] Updated properties.")
    global client
    global enabled
    global display_text
    global source_name
	
    if obs.obs_data_get_bool(settings, "enabled") is True:
        if (not enabled):
            if debug_mode: print("[HORN] Enabled mqtt client.")
            enabled = True
            ## here start mqqt client
            if client is not None:
                client.connect('UWR-Hupe', 1883, 60)
                client.loop_start()
    else:
        if (enabled):
            if debug_mode: print("[HORN] Disabled mqtt client.")
            enabled = False
            # here stop mqtt client
            if client is not None:
                client.loop_stop()
                client.disconnect()
			
    debug_mode = obs.obs_data_get_bool(settings, "debug_mode")
    for i in range(len(display_text)):
        display_text[i] = obs.obs_data_get_string(settings, "display_text_" + str(i))
        source_name[i] = obs.obs_data_get_string(settings, "source_name_" + str(i))

# ------------------------------------------------------------
# MQTT client functions
# ------------------------------------------------------------

def on_connect(client_local, userdata, flags, rc):
    global debug_mode
    if debug_mode: print("[HORN] Connected with result code " + str(rc) +
                         " and client " + str(client_local))
    if client_local is not None:
        client_local.subscribe("state/#", 1)
        if debug_mode: print("[HORN] Subscribed to 'state/#'")


def on_message(client_local, userdata, msg):
    global debug_mode
    if debug_mode: print("[HORN] New message: "
                         "topic=" + str(msg.topic) + " "
                         "payload=" + str(msg.payload))

    global display_text
    global source_name
    global old_outputtext

    global timetext
    global statustext
    global game_state
    global n_halftime
    global score_blue
    global score_white
    global teamname_blue
    global teamname_white

    try:
        if msg.topic == 'state/current_time':
            current_time = json.loads(msg.payload)
            ###########################################################
            # write in here what you want in the string for the time
            timetext = str(current_time['down'])
            #######################################################
        elif msg.topic == 'state/game_state':
            game_state = json.loads(msg.payload)
            n_halftime = str(game_state['n_halftime'])
            if game_state['is_timeout']:
                statustext = 'Auszeit'
            elif game_state['is_penaltythrow']:
                statustext = 'Strafwurf'
            elif game_state['is_halftime_break']:
                statustext = 'Halbzeitpause'
            elif game_state['is_match_over']:
                statustext = 'Beended'
            elif game_state['is_running']:
                statustext = ('Läuft')
            else:
                statustext = ('Unterbrochen')
        elif msg.topic == 'state/score_blue':
            score_blue = msg.payload.decode()
        elif msg.topic == 'state/score_white':
            score_white = msg.payload.decode()
        elif msg.topic == 'state/teamname_blue':
            teamname_blue = msg.payload.decode()
        elif msg.topic == 'state/teamname_white':
            teamname_white = msg.payload.decode()
        else:
            return
    except Exception as e:
        msg = ('The message caused an error:\n'
               'topic = ' + str(msg.topic) + '\n'
               'payload = ' + str(msg.payload) + '\n'
               'error msg = ' + str(e) + '\n\n' +
               traceback.format_exc())
        if debug_mode: print(msg)
        return

    for i in range(len(display_text)):

        outputtext = display_text[i]\
            .replace('%time', timetext)\
            .replace('%status', statustext)\
            .replace('%teamname_blue', str(teamname_blue))\
            .replace('%teamname_white', str(teamname_blue))\
            .replace('%score_blue', str(score_blue))\
            .replace('%score_white', str(score_white))\
            .replace('%n_halftime', str(n_halftime))
    
        if not old_outputtext[i] == outputtext:
            old_outputtext[i] = outputtext
            settings = obs.obs_data_create()
            obs.obs_data_set_string(settings, "text", outputtext)
            source = obs.obs_get_source_by_name(source_name[i])
            obs.obs_source_update(source, settings)
            obs.obs_data_release(settings)
            obs.obs_source_release(source)
            if debug_mode: print("[HORN] new output text " +
                                 str(i) +": " + outputtext)

    if debug_mode: print("[HORN] on_message has finished.")
