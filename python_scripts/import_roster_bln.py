#!/usr/bin/env python
# -*- coding: utf-8 -*-

from __future__ import print_function
import paho.mqtt.client as mqtt
import json
import os
import csv
import logging
import hornconfig
if not __name__ == '__main__':
    import hornconfig

Logger = logging.getLogger(__name__)

def send_roster(roster):
    client = mqtt.Client(callback_api_version=mqtt.CallbackAPIVersion.VERSION2)
    client.username_pw_set("GameController", hornconfig.mqtt_password)

    client.connect("localhost")
    client.loop_start()
    client.publish('tournament/roster', roster, retain=True)
    Logger.info("Roster sent successfully!")
    client.loop_stop()
    client.disconnect()

def import_roster():
    directory = os.path.join(os.path.dirname(os.path.realpath(__file__)), '..', 'team_lists')

    if not os.path.exists(directory):
        os.makedirs(directory)
    
    roster = {}

    team_placeholders = {
        "1": ["Berlin 1", "m", ""],
        "2": ["Bielefeld", "m", ""],
        "3": ["Dresden", "m", ""],
        "4": ["Hamburg", "m", ""],
        "5": ["Bremen", "m", ""],
        "6": ["Lübeck", "m", ""],
        "7": ["Göttingen", "m", ""],
        "8": ["Baltic United", "m", ""],
        "9": ["Stelle", "m", ""],
        "10": ["Rostock", "m", ""],
        "11": ["Paderborn", "m", ""],
        "12": ["Berlin 2", "m", ""],
    }
    files = []
    for val in team_placeholders.values():
        f = os.path.join(directory, val[0] + '.csv')
        files.append(f)

    for i, f in enumerate(files):
        try:
            with open(f) as file:
                firstLine = file.readline().strip().strip(",").split(",")
                teamname = firstLine[0]
                gender = firstLine[1]
                roster[str(i + 1)] = {"name": teamname,
                                      "nation": "",
                                      "gender_category": gender,
                                      "players": {}}
                for line in file:
                    if line.strip() == "":
                        continue
                    player = line.strip().split(',')
                    if len(player) == 1:
                        continue
                    elif len(player) == 2:
                        roster[str(i + 1)]["players"][player[0]] = {"name": player[1],
                                                                "passnumber": ""}
                    elif len(player) == 3:
                        roster[str(i + 1)]["players"][player[0]] = {"name": player[1],
                                                                "passnumber": player[2]}
                
        except FileNotFoundError:
            Logger.info('No lists found, adding placeholders without players')
            for i in range(len(team_placeholders)):
                roster[str(i+1)] = {"name": team_placeholders[str(i+1)][0], 
                                    "nation": team_placeholders[str(i+1)][2], 
                                    "gender_category": team_placeholders[str(i+1)][1],
                                    "players": {}}

    return roster


if __name__ == '__main__':
    root_logger = logging.getLogger()
    root_logger.setLevel(logging.INFO)
    log_formatter = logging.Formatter("[%(levelname)s][%(name)s]"
                                      "[%(module)s][%(funcName)s]"
                                      "[%(threadName)s]%(message)s")
    console_handler = logging.StreamHandler()
    console_handler.setFormatter(log_formatter)
    root_logger.addHandler(console_handler)

    roster = import_roster()
    send_roster(json.dumps(roster))
